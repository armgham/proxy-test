FROM node:slim

#WORKDIR /home/jsmtproxy

RUN apt-get update && apt-get install -y git
RUN git clone https://github.com/FreedomPrevails/JSMTProxy.git

RUN npm install pm2 -g

#EXPOSE 6565

#ARG MTPROTO_SECRET
#ENV MTPROTO_SECRET ${MTPROTO_SECRET:-DEADBEEFDEADBEEFDEADBEEF}
#RUN sed -i "s/11111111111111111111111111111111/${MTPROTO_SECRET}/g" config.json

COPY config.json .

CMD [ "pm2", "start", "mtproxy.js", "-i", "max"]